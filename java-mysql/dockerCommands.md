# MYSQL as a standalone container

## start mysql container using docker
```sh
docker run -p 3306:3306 \
--name mysql \
-e MYSQL_ROOT_PASSWORD=rootpass \
-e MYSQL_DATABASE=team-member-projects \
-e MYSQL_USER=admin \
-e MYSQL_PASSWORD=adminpass \
-d mysql mysqld --default-authentication-plugin=mysql_native_password
```
## create java jar file

./gradlew build

## set env vars in local terminal for the java application (these will read in DatabaseConfig.java)

`export DB_USER=admin`

`export DB_PWD=adminpass`

`export DB_SERVER=localhost`

`export DB_NAME=team-member-projects`

## start java application

`java -jar build/libs/bootcamp-java-mysql-project-1.0-SNAPSHOT.jar`

# Starting PHPMYADMIN for managing MYSQL DB container

```shell
docker run -p 8083:80 \
--name phpmyadmin \
--link mysql:db \
-d phpmyadmin/phpmyadmin
```




# Docker-compose file

```sh
version: '3'
services:
mysql:
image: mysql
ports:
- 3306:3306
environment:
- MYSQL_ROOT_PASSWORD=rootpass
- MYSQL_DATABASE=team-member-projects
- MYSQL_USER=admin    
- MYSQL_PASSWORD=adminpass
volumes:
- mysql-data:/var/lib/mysql
container_name: mysql
command: --default-authentication-plugin=mysql_native_password
phpmyadmin:
image: phpmyadmin
environment:
- PMA_HOST=mysql
ports:
- 8083:80
container_name: phpmyadmin
volumes:
mysql-data:
driver: local

```


