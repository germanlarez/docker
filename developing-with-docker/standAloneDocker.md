# Commands for stand alone mongo and mongo express services

### Creating network

`docker network create mongo-network`

### Running mongo DB

`docker run -d --name mongodb -p 27017:27017 --net mongo-network -e MONGO_INITDB_ROOT_USERNAME=admin -e MONGO_INITDB_ROOT_PASSWORD=password mongo`

### Running mongo DB's UI

`docker run -d --name mongo-express -p 8081:8081 --net mongo-network   -e ME_CONFIG_MONGODB_SERVER=mongodb -e ME_CONFIG_MONGODB_ADMINUSERNAME=admin -e ME_CONFIG_MONGODB_ADMINPASSWORD=password mongo-express`


